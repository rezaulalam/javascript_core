var people = [

{
	"name":"Alchemi", "born":1876, "sex":"male", "died":1956
},

{
	"name":"Natasha", "born":1901, "sex":"female", "died":1932
},

{
	"name":"Norman", "born":1856, "sex":"male", "died":1916
},

{
	"name":"Alasad", "born":1906, "sex":"male", "died":1976
}

]


function filter(array, test){

	var passed = [];

	for (var i = 0; i < array.length; i++) {
		if (test(array[i])) {
			passed.push(array[i].name);
		}
	}

	return passed;
}

console.log(filter(people, function(person){
	return person.born> 1900 && person.born<1925;
}));



