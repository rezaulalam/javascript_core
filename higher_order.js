function greaterThan(n){

	return function(m){
		return m>n;
	}

}

var greaterThan10 = greaterThan(10);

console.log(greaterThan10(11));


function noise(f){

	return function(arg){

		console.log("What is that: "+f*arg);
	}

}

noise(3)(5);



function repeat (times, body) {
	for (var i = 0; i < times; i++) {
		body(i);
	};
}


function unless (evenTest, sayType) {
	if (!evenTest) {

			console.log(!evenTest);
			sayType();

	}
}

repeat(3, function (n) {
	unless(n%2, function () {
		console.log(n, "is even");
	});
});






