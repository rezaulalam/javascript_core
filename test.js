function multiply (factor) {
	// body...
	var hello = factor * 2;

	return function(number){

	return number * hello;

	}
}

var newString = "hello World";
var upperCaseString = newString.toUpperCase();

console.log(upperCaseString);

var tryFunc = multiply(20);

var testObj = {name:"sajib", age:26 , grade:{cgpa:["A+","A"] , gpa:[4,3.5]  }};

console.log(testObj.grade.cgpa[1]+" "+testObj.grade.gpa[1]);

console.log(tryFunc(5));

var journal = [
{events:["work","touched tree","pizza","running","television"], squirrel:true},
{events:["fan","ice cream","swimming","sleep","cricker"], squirrel:false},
{events:["pen","sky","hello","night","sand"], squirrel:true},
];

console.log(journal[1].events+" "+journal[1].squirrel);

var idea = [];

function addIdea(events, isActive){

	idea.push({
		event:events, 
		activity:isActive,
	});
}

addIdea(["play","game","in","the","field"],false);
addIdea(["swiming","in","the","river"],true);
addIdea(["Riding","bycycle","on","the", "beach"],false);

//console.log(idea);

var map = {};

function storeData(event, value){
	map[event] = value;
}
	storeData("hello",8);
	storeData("world",5);

	console.log(map["hello"]);
	console.log("world" in map);


