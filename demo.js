console.log(sum(range(1,10)));

function range(start,end,step){

	if (!step) {step=1;}

	var r = [];

	for (var i = start; i <= end; i++) {
		r.push(i);
	}

	return r;
}


function sum(arr){

	var total = 0;
	for (var i = 0; i < arr.length; i++) {
		total = total + arr[i];
	}

	return total;

}